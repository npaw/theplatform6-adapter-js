## [6.7.1] 2020-07-21
### Fixed
- Ad preroll detection when replaying

## [6.7.0] 2020-07-02
### Fixed
- Ad preroll detection
### Library
- Packaged with `lib 6.7.11`

## [6.5.6] 2019-10-28
### Added
- New bitrate getter, and rendition too
### Library
- Packaged with `lib 6.5.18`

## [6.5.5] 2019-08-01
### Fixed
- Access to isAd for pdk version 6

## [6.5.4] 2019-07-31
### Added
- Options to be added using tp:plugin parameters
### Library
- Packaged with `lib 6.5.9`

## [6.5.3] 2019-07-26
### Fixed
- Various issues related with monitor
### Library
- Packaged with `lib 6.5.8`

## [6.5.2] 2019-07-04
### Fixed
- Multiple seek issue for many browsers, mostly on windows 10

## [6.5.1] 2019-06-04
### Fixed
- Ad tracking when releaseSelected event is not triggered

## [6.5.0] 2019-05-31
### Library
- Packaged with `lib 6.5.2`

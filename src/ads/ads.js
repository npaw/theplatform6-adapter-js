/* global $pdk */
var youbora = require('youboralib')
var manifest = require('../../manifest.json')
const { Log } = require('youboralib')

var AdsAdapter = youbora.Adapter.extend({

  constructor: function (player, scope) {
    this.scope = scope
    youbora.adapters.ThePlatform.AdsAdapter.__super__.constructor.call(this, player)
  },

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead || 0
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.duration
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate
  },

  /** Override to return title */
  getTitle: function () {
    return this.title
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.resource
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    var pdk = $pdk
    var version = pdk ? pdk.version.toString() : ''
    if (version === '[object Object]') {
      if (pdk.version.major && pdk.version.minor && pdk.version.revision && pdk.version.build) {
        version = pdk.version.major + '.' + pdk.version.minor + '.' + pdk.version.revision + '.' + pdk.version.build
      }
    }
    return version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'ThePlatform'
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    return this.adPosition
  },

  getGivenBreaks: function () {
    var ret = null
    if (this.breaks) {
      ret = this.breaks.length
    }
    return ret
  },

  getBreaksTime: function () {
    return this.breakTime
  },

  getGivenAds: function () {
    return 1
  },

  getIsSkippable: function () {
    var ret = null
    if (this.actualAd) {
      ret = !this.actualAd.noSkip
    }
    return ret
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    // Register listeners
    this.references = {
      OnMediaLoadStart: this.loadListener.bind(this),
      OnMediaError: this.errorListener.bind(this),
      OnMediaEnd: this.endListener.bind(this),
      OnMediaPlaying: this.playingListener.bind(this),
      OnMediaPause: this.pauseListener.bind(this),
      OnMediaUnpause: this.unpauseListener.bind(this),
      OnMediaClick: this.clickListener.bind(this),
      OnReleaseStart: this.releaseStartListener.bind(this)
    }
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key], this.scope)
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  /** Listener for 'OnMediaLoadStart' event. */
  loadListener: function (e) {
    if (e.data.clip) {
      this.bc = e.data.clip.baseClip || e.data.clip
    } else {
      this.bc = e.data.baseClip
    }
    this.actualAd = this.bc
    if (this.bc.isAd) {
      this.duration = (this.bc.releaseLength || this.bc.length) / 1000
      this.bitrate = this.bc.bitrate || 0
      this.resource = this.bc.URL
      this.title = e.data.title
      if (this.bc.moreInfo) this.clickURL = this.bc.moreInfo.href
      this.adPosition = youbora.Constants.AdPosition.Preroll
      if (this.plugin.getAdapter().getDuration() <= this.plugin.getAdapter().getPlayhead() + 2) {
        this.adPosition = youbora.Constants.AdPosition.Postroll
      } else if (this.plugin.getAdapter().flags.isJoined) {
        this.adPosition = youbora.Constants.AdPosition.Midroll
      }
      this.plugin.getAdapter().firePause()
      this.fireStart()
      this._quartileTimer = new youbora.Timer(this.sendQuartile.bind(this), 1000)
      this._quartileTimer.start()
    } else {
      this.fireStop()
      this.fireBreakStop()
    }
  },

  sendQuartile: function (e) {
    var quartile = 0
    if (this.playhead > this.duration / 4) {
      if (!this.firstQuartile) {
        this.firstQuartile = true
        quartile = 1
      }
      if (this.playhead > this.duration / 2) {
        if (!this.secondQuartile) {
          this.secondQuartile = true
          quartile = 2
        }
        if (this.playhead > this.duration * 0.75) {
          if (!this.thirdQuartile) {
            this.thirdQuartile = true
            quartile = 3
          }
        }
      }
    }
    this.fireQuartile(quartile)
    if (quartile === 3) this._quartileTimer.stop()
  },

  /** Listener for 'OnReleaseStart' event. */
  releaseStartListener: function (e) {
    this.adlist = e.data.clips
    if (this.adlist) {
      this.breaks = []
      this.breakTime = []
      this.timeacum = 0
      var started = false
      for (var clip in this.adlist) {
        var bc = this.adlist[clip].baseClip || this.adlist[clip]
        if (bc.isAd) {
          if (started) {
            this.breaks[this.breaks.length - 1]++
          } else {
            this.breaks.push(1)
            this.breakTime.push(this.timeacum)
          }
        } else {
          this.timeacum = bc.endTime / 1000
          started = false
        }
      }
    }
  },

  /** Listener for 'OnMediaError' event. */
  errorListener: function (e) {
    if (this.flags.isStarted || this.flags.isInited) {
      this.fireError(e.data.friendlyMessage || e.data.message)
      this.fireStop()
      this.plugin.getAdapter().fireResume()
      this.plugin.getAdapter().monitor.skipNextTick()
    }
  },

  /** Listener for 'OnMediaEnd' event. */
  endListener: function (e) {
    this.fireStop()
    this.plugin.getAdapter().fireResume()
    this.plugin.getAdapter().monitor.skipNextTick()
    this.resetValues()
  },

  /** Listener for 'OnMediaPlaying' event. */
  playingListener: function (e) {
    if (this.flags.isStarted || this.flags.isInited) {
      this.playhead = (e.data.currentTimeAggregate ? e.data.currentTimeAggregate : e.data.currentTime) / 1000
      this.fireJoin()
      this.monitor.skipNextTick()
    }
  },

  /** Listener for 'OnMediaPause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'OnMediaUnpause' event. */
  unpauseListener: function (e) {
    this.fireResume()
  },

  clickListener: function (e) {
    if (this.flags.isStarted) {
      this.fireClick(this.clickURL)
    }
  },

  resetValues: function () {
    this.resource = null
    this.title = null
    this.duration = null
    this.playhead = null
    this.duration = null
    this.bitrate = null
    this.adPosition = null
    this.clickURL = null
    this.firstQuartile = false
    this.secondQuartile = false
    this.thirdQuartile = false
  }
})

module.exports = AdsAdapter

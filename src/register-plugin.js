/* global $pdk */
var youbora = require('youboralib')
require('./adapter')

if ($pdk && $pdk.controller && $pdk.controller.plugInLoaded) {
  var youboraPlugIn = {
    initialize: function (lo) {
      if (lo.vars.accountCode) {
        var scope = lo.vars.scope || undefined
        window.youboraAnalytics = new youbora.Plugin({ accountCode: lo.vars.accountCode })
        if (lo.vars.session) {
          window.youboraAnalytics.infinity.begin()
        }
        if (lo.vars.extraparams) {
          window.youboraAnalytics.options.setCustomDimensions(lo.vars.extraparams.split(','))
        }
        window.youboraAnalytics.setAdapter(new youbora.adapters.ThePlatform(lo.controller, scope))
        window.youboraAnalytics.setAdsAdapter(new youbora.adapters.ThePlatform.AdsAdapter(lo.controller, scope))
      }
    }
  }

  $pdk.controller.plugInLoaded(youboraPlugIn)
}

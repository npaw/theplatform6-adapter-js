/* global $pdk */
var youbora = require('youboralib')
var manifest = require('../manifest.json')
const { Log } = require('youboralib')

youbora.adapters.ThePlatform = youbora.Adapter.extend({

  constructor: function (player, options) {
    if (options && options.accountCode) {
      this.options = options
      this.scope = this.options.scope
    } else {
      this.scope = options
    }
    youbora.adapters.ThePlatform.__super__.constructor.call(this, player)
  },

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.duration
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate
  },

  /** Override to return rendition */
  getRendition: function () {
    var ret = null
    if (this.bitrate > 0) {
      ret = youbora.Util.buildRenditionString(this.width, this.height, this.bitrate)
    }
    return ret
  },

  /** Override to return title */
  getTitle: function () {
    return this.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.isLive
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.resource
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    var pdk = $pdk
    var version = pdk ? pdk.version.toString() : ''
    if (version === '[object Object]') {
      if (pdk.version.major && pdk.version.minor && pdk.version.revision && pdk.version.build) {
        version = pdk.version.major + '.' + pdk.version.minor + '.' + pdk.version.revision + '.' + pdk.version.build
      }
    }
    return version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'ThePlatform'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player, [null,
      'OnMediaPlaying',
      'OnMediaPause',
      'OnMediaUnpause',
      'OnClipInfoLoaded',
      'OnMediaBuffering',
      'OnMediaEnd',
      'OnMediaError',
      'OnVersionError',
      'OnMediaPlay',
      'OnMediaSeek',
      'OnMediaStart',
      'OnMediaLoadStart',
      'OnResetPlayer',
      'OnReleaseEnd',
      'OnReleaseStart',
      'OnReleaseSelected'
    ])

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)

    // Register listeners
    this.references = {
      OnReleaseStart: this.releaseStartListener.bind(this),
      OnMediaLoadStart: this.mediaLoadListener.bind(this),
      OnRenditionSwitched: this.renditionListener.bind(this),
      OnMediaPlaying: this.playingListener.bind(this),
      OnMediaPause: this.pauseListener.bind(this),
      OnMediaUnpause: this.unpauseListener.bind(this),
      OnReleaseEnd: this.releaseEndListener.bind(this),
      OnMediaError: this.mediaErrorListener.bind(this),
      OnMediaSeek: this.mediaSeekListener.bind(this),
      OnReleaseSelected: this.releaseSelected.bind(this)
    }

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key], this.scope)
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = []
    }
  },

  releaseSelected: function (e) {
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.ThePlatform.AdsAdapter(this.player, this.scope))
    }
  },

  /** Listener for 'OnReleaseStart' event. */
  releaseStartListener: function (e) {
    var clips = e.data.baseClips || e.data.clips
    if (clips) {
      var clip
      for (var clipNumber in clips) {
        clip = clips[clipNumber]
        if (!clip.isAd) {
          this.resource = clip.URL
          this.title = clip.title
          if (!this.title) {
            if (e.data.title) this.title = e.data.title
          }
          this.duration = (clip.trueLength || clip.length) / 1000
          this.isLive = (clip.expression === 'nonstop')
          this.bitrate = clip.bitrate || 0
          if (this.bitrate > 6e7) this.bitrate /= 1000
          break
        }
      }
    }
    for (var i = 0; i < 20; i++) {
      if ((this.plugin.options['content.customDimension.' + (i + 1)] === '{tp_aid}')) {
        this.plugin.options['content.customDimension.' + (i + 1)] = e.data.accountID
        break
      }
    }
    this.fireStart()
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.ThePlatform.AdsAdapter(this.player, this.scope))
    }
    this.plugin.getAdsAdapter().releaseStartListener(e)
  },

  /** Listener for 'OnMediaLoadStart' event. */
  mediaLoadListener: function (e) {
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.ThePlatform.AdsAdapter(this.player, this.scope))
      this.plugin.getAdsAdapter().loadListener(e)
    }
    var bc = null
    if (e.data.clip) {
      bc = e.data.clip.baseClip || e.data.clip
    } else {
      bc = e.data
    }
    this.lastBc = bc
    if (bc.contentCustomData && bc.contentCustomData.isException) {
      this.fireError(bc.contentCustomData.responseCode, bc.contentCustomData.exception)
    } else if (!bc.isAd) {
      this.duration = (bc.trueLength || bc.length) / 1000
      this.bitrate = bc.bitrate || 0
      if (this.bitrate > 6e7) this.bitrate /= 1000
    }
  },

  /** Listener for 'OnRenditionSwitched' event. */
  renditionListener: function (e) {
    if (e.newRendition) {
      this.bitrate = e.newRendition
    } else if (e.data && e.data.newRendition && e.data.newRendition.bitrate) {
      this.bitrate = e.data.newRendition.bitrate
      this.width = e.data.newRendition.width
      this.height = e.data.newRendition.height
    }
  },

  /** Listener for 'OnMediaPlaying' event. */
  playingListener: function (e) {
    this.fireStart()
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.ThePlatform.AdsAdapter(this.player, this.scope))
      this.plugin.getAdsAdapter().fireStart()
      this.plugin.getAdsAdapter().playingListener(e)
    } else if (this.lastBc.isAd) {
      this.plugin.getAdsAdapter().fireStart()
      this.plugin.getAdsAdapter().fireJoin()
    }
    if (!this.plugin.getAdsAdapter() || (this.plugin.getAdsAdapter() && !this.plugin.getAdsAdapter().flags.isStarted)) {
      this.playhead = (e.data.currentTimeAggregate ? e.data.currentTimeAggregate : e.data.currentTime) / 1000
      if (!this.flags.isBuffering) {
        this.fireResume()
        if (!this.flags.isJoined && !this.plugin.getAdsAdapter().flags.isStarted) {
          this.fireJoin()
          this.monitor.skipNextTick()
        }
      }
    }
  },

  /** Listener for 'OnMediaPause' event. */
  pauseListener: function (e) {
    if (!this.plugin.getAdsAdapter() || (this.plugin.getAdsAdapter() && !this.plugin.getAdsAdapter().flags.isStarted)) {
      this.firePause()
    }
  },

  /** Listener for 'OnMediaUnpause' event. */
  unpauseListener: function (e) {
    var bc = e.data.clip.baseClip || e.data.clip
    this.lastBc = bc
    if (!bc.isAd) {
      if (!this.plugin.getAdsAdapter() || (this.plugin.getAdsAdapter() && !this.plugin.getAdsAdapter().flags.isStarted)) {
        this.fireResume()
      }
      if (!this.flags.isStarted && !this.plugin.getAdsAdapter().flags.isStarted) {
        this.fireStart()
        this.fireJoin()
      }
    }
  },

  /** Listener for 'OnReleaseEnd' event. */
  releaseEndListener: function (e) {
    this.fireStop() // should be called when going to background with phones
    this.resetValues()
  },

  /** Listener for 'OnMediaError' event. */
  mediaErrorListener: function (e) {
    var bc = e.data.clip.baseClip || e.data.clip
    this.lastBc = bc
    if (!bc.isAd) {
      this.resource = bc.URL
      this.title = bc.title
      this.duration = (bc.mediaLength || bc.length) / 1000
      this.fireError(e.data.friendlyMessage || e.data.message)
      this.fireStop()
    }
  },

  /** Listener for 'OnMediaSeek' event. */
  mediaSeekListener: function (e) {
    this.fireSeekBegin()
    this.seekingTo = (e.data.end.currentTimeAggregate ? e.data.end.currentTimeAggregate : e.data.end.currentTime) / 1000
  },

  resetValues: function () {
    this.resource = null
    this.title = null
    this.duration = null
    this.seekingTo = null
    this.playhead = null
    this.duration = null
    this.bitrate = null
    this.isLive = null
  }
},
{
  AdsAdapter: require('./ads/ads')
}
)

module.exports = youbora.adapters.ThePlatform
